$(document).on 'click', 'form .remove_order_items', (event) ->
  $(this).prev('input[type=hidden]').val('1')
  $(this).closest('fieldset').hide()
  event.preventDefault()

$(document).on 'click', 'form .add_order_items', (event) ->
  time = new Date().getTime()
  regexp = new RegExp($(this).data('id'), 'g')
  $(this).before($(this).data('fields').replace(regexp, time))
  event.preventDefault()

jQuery ->
  $(document).on "nested:fieldAdded", (event) ->
    $( ".order_items" ).each ->
      $(this).find('#product_id').bind "change", ->
        product = $(this).parent().find('#product_id :selected').text()
        console.log("Test")
        if (type == "km")
          $(this).parent().find('#payment_method').addClass('hidden').hide()              
          $(this).parent().find('#amount_in_currency').addClass('hidden').hide()
          $(this).parent().find('#amount_currency').addClass('hidden').hide()
          $(this).parent().find('#km_traveled').removeClass('hidden').show()
        else
          $(this).parent().find('#payment_method').removeClass('hidden').show()
          $(this).parent().find('#amount_currency').removeClass('hidden').show()
          $(this).parent().find('#amount_in_currency').removeClass('hidden').show()
          $(this).parent().find('#km_traveled').addClass('hidden').hide()
      $(this).find('#expense_type').trigger('change')

  $(document).trigger("nested:fieldAdded")