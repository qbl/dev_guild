class AddVariantIdToOrderItem < ActiveRecord::Migration[5.0]
  def change
    add_column :order_items, :variant_id, :integer
  end
end
